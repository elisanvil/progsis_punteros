#include <stdio.h>
#define MAX 10

static char caracteres[MAX] = {'a','b','c'};
static long enteros_largos[MAX];
static char *punteros[MAX] = {"hola","mundo","hello","world"};

void iterar_enteros(int *);
void iterar_caracteres(char *);
void iterar_enteros_largos(long *);
void iterar_punteros(char **);

int main()
{
	int enteros[MAX];
	enteros[0] = 100;
	enteros[1] = 300;
	enteros[5] = 50;

	enteros_largos[0] = 10000;
	enteros_largos[1] = 30000;
	enteros_largos[5] = 50000;

	iterar_enteros(enteros);
	iterar_enteros_largos(enteros_largos);
	iterar_caracteres(caracteres);
	iterar_punteros(punteros);
}

void iterar_enteros(int *enteros)
{
	int *p_anterior = NULL;
	int *p;
	for(int i=0;i<MAX;i++)
	{
		p = enteros + i;
		
		printf("\n*Enteros*\n");
		printf("El valor de p es: %d\n", *p);
		printf("La dirección de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n",(long)p - (long)p_anterior);

		p_anterior = p;
	}
}

void iterar_enteros_largos(long *enteros_largos)
{
	long *p_anterior = NULL;
	long *p;
	for(int i=0;i<MAX;i++)
	{
		p = enteros_largos + i;
		
		printf("\n\n*Enteros Largos*\n\n");
		printf("El valor de p es: %ld\n", *p);
		printf("La dirección de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n",(long)p - (long)p_anterior);

		p_anterior = p;
	}
}

void iterar_caracteres(char *caracteres)
{
	char *p_anterior = NULL;
	char *p;
	for(int i=0;i<MAX;i++)
	{
		p = caracteres + i;
		
		printf("\n\n*Caracteres*\n\n");
		printf("El valor de p es: %c\n", *p);
		printf("La dirección de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n",(long)p - (long)p_anterior);

		p_anterior = p;
	}
}

void iterar_punteros(char **puntero)
{
	char **p_anterior = NULL;
	char **p;
	for(int i=0;i<MAX;i++)
	{
		p = puntero + i;
		
		printf("\n\n*Punteros*\n\n");
		printf("El valor de p es: %s\n", *p);
		printf("La dirección de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n",(long)p - (long)p_anterior);

		p_anterior = p;
	}
}

/*

  ¿Cuál es el efecto de añadir la palabra clave static a la declaración del arreglo enteros en la línea 15 de main.c?
  Al usar static, se reserva memoria y se inicializa en cero en el momento de la compilación. Sin la palabra static, en las
  direcciones de memoria habrán datos basura.

  Al implementar las otras funciones, el offset entre direcciones es diferente, ¿que representa en realidad el valor de este offset?
  Representa el tamaño de bytes según el tipo de declaración, ya sea int (4), long (8), char(1), etc. en una arquitectura de 64 bits.

  En la línea 33 del archivo main.c, ¿por qué es necesario hacer un casting a long?
  Al hacer este casting, se forza al compilador a no realizar aritmética de punteros.
  Ya no se toma en cuenta la dirección de memoria de la variable como una dirección, sino como un dato de tipo long.

*/

